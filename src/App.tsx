import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Grid } from "./components/Grid"

interface AppProps { }

function App({ }: AppProps) {
  // Return the App component.
  return (
    <Grid />
  );
}

export default App;
