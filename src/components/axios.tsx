import axios from "axios"

const instance = axios.create({
  baseURL: "https://hipsum.co/api"
})
export default instance
