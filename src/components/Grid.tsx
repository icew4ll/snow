import React, { useState, useEffect } from "react"
import axios from "./axios"

export const Grid = () => {
  const [ipsum, setIpsum] = useState([])
  const URL = "/?type=hipster-centric&paras=15"

  useEffect(() => {
    // async requests must be wrapped in a function and called
    async function fetchData() {
      const req = await axios.get(URL)
      setIpsum(req.data)
      return req
    }
    fetchData()
    // [] blank brackets = run on load once
    // [URL] blank brackets = run on load and when variable changes
    // any variables required to process result should invoke rerender via [VARIABLE]
  }, [URL])

  console.log(JSON.stringify(ipsum, undefined, 4))

  return (
    <div>
      <div className="fixed w-full grid grid-cols-3 gap-1">
        <div className="col-span-3 md:col-span-1 md:col-end-4 bg-blue-900">
          3
        </div>
      </div>
      <div className="grid grid-cols-3 gap-4">
        <div className="col-span-3 md:col-span-2 bg-red-900">
          {ipsum.map((i, index): JSX.Element => (
            <p
              key={index}>
              {i}
            </p>
          ))}
        </div>
      </div>
    </div>
  )
}
